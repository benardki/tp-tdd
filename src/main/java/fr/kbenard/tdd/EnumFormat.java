package fr.kbenard.tdd;

public enum EnumFormat {
    POCHE("Poche"),
    BROCHÉ("Broché"),
    GRAND_FORMAT("Grand format");

    private String nom;

    EnumFormat(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }
}
