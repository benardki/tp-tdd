package fr.kbenard.tdd;

public interface BookDataService {

    Book getBookData(String isbn);

    void addBook(Book book);

    void updateBook(Book book);

    void deleteBook(String isbn);
}
