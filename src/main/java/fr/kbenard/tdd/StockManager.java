package fr.kbenard.tdd;

public class StockManager {

    private BookDataService databaseBookService;
    private BookDataService webBookService;

    public BookDataService getWebBookService() {
        return webBookService;
    }

    public void setWebBookService(BookDataService webBookService) {
        this.webBookService = webBookService;
    }

    public BookDataService getDatabaseBookService() {
        return databaseBookService;
    }

    public void setDatabaseBookService(BookDataService databaseBookService) {
        this.databaseBookService = databaseBookService;
    }

    public String getLocator(String isbn) {
        if (!new ISBNValidator().validateISBN(isbn)) throw new InvalidISBNException();

        Book book = databaseBookService.getBookData(isbn);
        if (book == null) {
            book = webBookService.getBookData(isbn);
        }
        if (book == null) throw new BookNotFoundException();
        String locator = isbn.substring(isbn.length() - 4) +
                book.getAuthor().charAt(0) +
                book.getTitle().split(" ").length;
        return locator;
    }

    public void addBookToDatabase(Book book) {
        databaseBookService.addBook(book);
    }

    public void updateBookInDatabase(Book book) {
        databaseBookService.updateBook(book);
    }

    public void deleteBookFromDatabase(String isbn) {
        databaseBookService.deleteBook(isbn);
    }
}

