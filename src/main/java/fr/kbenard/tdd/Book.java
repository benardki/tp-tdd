package fr.kbenard.tdd;

public class Book {

    private String isbn;
    private String title;
    private String author;
    private String editor;
    private EnumFormat format;
    private Boolean isAvailable;

    public Book(String isbn, String title, String author, String editor, EnumFormat format, Boolean isAvailable) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.editor = editor;
        this.format = format;
        this.isAvailable = isAvailable;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getEditor() {
        return editor;
    }

    public EnumFormat getFormat() {
        return format;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }
}
