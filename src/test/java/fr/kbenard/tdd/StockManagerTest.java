package fr.kbenard.tdd;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static fr.kbenard.tdd.EnumFormat.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class StockManagerTest {

    BookDataService mockDbService;
    BookDataService mockWebService;

    StockManager manager;

    @BeforeEach
    public void initMocks() {
        mockDbService = mock(BookDataService.class);

        mockWebService = mock(BookDataService.class);

        manager = new StockManager();
        manager.setDatabaseBookService(mockDbService);
        manager.setWebBookService(mockWebService);
    }

    @Test
    public void canGetCorrectLocator() {
        String isbn = "2379890943";

        when(mockWebService.getBookData(isbn)).thenReturn(new Book(isbn, "Animal Crossing 100 NOUVEAUX trucs à savoir pour bâtir son petit coin de paradis", "Filipe Canelas", "Omake books", POCHE, true));

        String expectedLocator = "0943F14";
        String locator = manager.getLocator(isbn);
        assertEquals(expectedLocator, locator);
    }

    @Test
    public void databaseIsUsedIfDataPresent() {
        String isbn = "2379890943";
        when(mockDbService.getBookData(isbn)).thenReturn(new Book(isbn, "Animal Crossing 100 NOUVEAUX trucs à savoir pour bâtir son petit coin de paradis", "Filipe Canelas", "Omake books", POCHE, true));

        manager.getLocator(isbn);

        verify(mockDbService).getBookData(isbn);
        verifyNoInteractions(mockWebService);
    }

    @Test
    public void webserviceIsUsedIfDataNotPresent() {
        String isbn = "2379890943";
        when(mockWebService.getBookData(isbn)).thenReturn(new Book(isbn, "Animal Crossing 100 NOUVEAUX trucs à savoir pour bâtir son petit coin de paradis", "Filipe Canelas", "Omake books", POCHE, true));

        manager.getLocator(isbn);

        verify(mockDbService).getBookData(isbn);
        verify(mockWebService).getBookData(isbn);
    }

    @Test
    public void canAddBookToDatabase() {
        String isbn = "2379890943";
        Book book = new Book(isbn, "Animal Crossing 100 NOUVEAUX trucs à savoir pour bâtir son petit coin de paradis", "Filipe Canelas", "Omake books", POCHE, true);

        when(mockDbService.getBookData(isbn)).thenReturn(null); // Book not found in database
        when(mockWebService.getBookData(isbn)).thenReturn(null); // Book not found in web service

        manager.addBookToDatabase(book);

        verify(mockDbService).addBook(book);
    }

    @Test
    public void canUpdateBookInDatabase() {
        String isbn = "2379890943";
        Book book = new Book(isbn, "Animal Crossing 100 NOUVEAUX trucs à savoir pour bâtir son petit coin de paradis", "Filipe Canelas", "Omake books", POCHE, true);

        when(mockDbService.getBookData(isbn)).thenReturn(book); // Book found in database

        manager.updateBookInDatabase(book);

        verify(mockDbService).updateBook(book);
    }

    @Test
    public void canDeleteBookFromDatabase() {
        String isbn = "2379890943";
        Book book = new Book(isbn, "Animal Crossing 100 NOUVEAUX trucs à savoir pour bâtir son petit coin de paradis", "Filipe Canelas", "Omake books", POCHE, true);

        when(mockDbService.getBookData(isbn)).thenReturn(book); // Book found in database

        manager.deleteBookFromDatabase(isbn);

        verify(mockDbService).deleteBook(isbn);
    }

    @Test
    public void canGetBookFromDatabase() {
        String isbn = "2379890943";
        Book book = new Book(isbn, "Animal Crossing 100 NOUVEAUX trucs à savoir pour bâtir son petit coin de paradis", "Filipe Canelas", "Omake books", POCHE, true);

        when(mockDbService.getBookData(isbn)).thenReturn(book); // Book found in database

        Book result = manager.getBookFromDatabase(isbn);

        assertEquals(book, result);
        verify(mockDbService).getBookData(isbn);
    }

    @Test
    public void throwExceptionWhenBookNotFound() {
        assertThrows(BookNotFoundException.class, () -> manager.getLocator("2379890943"));
    }

    @Test
    public void throwExceptionWhenInvalidIsbn() {
        assertThrows(InvalidISBNException.class, () -> manager.getLocator("2379890946"));
    }
}